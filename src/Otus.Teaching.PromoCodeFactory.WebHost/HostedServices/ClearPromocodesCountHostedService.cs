﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.WebHost.HostedServices
{
    public class ClearPromocodesCountHostedService
        : BackgroundService
    {
        private readonly ILogger<ClearPromocodesCountHostedService> _logger;
        private readonly IServiceScopeFactory _serviceScopeFactory;

        public ClearPromocodesCountHostedService(ILogger<ClearPromocodesCountHostedService> logger, 
            IServiceScopeFactory serviceScopeFactory)
        {
            _logger = logger;
            _serviceScopeFactory = serviceScopeFactory;
        }
        

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Сервис начинает работу");

            await BackgroundProcessing(stoppingToken);
        }

        private async Task BackgroundProcessing(CancellationToken stoppingToken)
        {
            using var scope = _serviceScopeFactory.CreateScope();
            var employeeRepository = scope.ServiceProvider.GetService<IRepository<Employee>>();

            var employees = await employeeRepository.GetAllAsync();
            
            foreach (var employee in employees)
            {
                employee.AppliedPromocodesCount = 0;
                await employeeRepository.UpdateAsync(employee);
            }
            
            _logger.LogInformation("Обнулили промокоды");
            
            await Task.Delay(10000, stoppingToken);
        }

        public override async Task StopAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Сервис останавливается");

            await base.StopAsync(stoppingToken);
        }
    }
}